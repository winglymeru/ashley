﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Workshop
{
    public class Invoice
    {
        private WorkshopEntity _workshop { get;  set; }
        private LocationEntity _location { get; set; }

        public Invoice (WorkshopEntity workshop, LocationEntity location)
        {
            this._workshop = workshop;
            this._location = location;
        }
        
        public decimal Total
        {
            get
            {
                if(this._workshop == null || this._location == null)
                {
                    return 0;
                }
                return (this._workshop.Duration * this._location.Fees) + this._workshop.Fees ;
            }
        }

    }
}
