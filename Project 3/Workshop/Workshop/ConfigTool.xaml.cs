﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Workshop
{
    /// <summary>
    /// Interaction logic for ConfigTool.xaml
    /// </summary>
    public partial class ConfigTool : Window
    {
        private string pathWorkshop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Workshops.json";
        private string pathLocation = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Location.json";

        public List<WorkshopEntity> Workshops { get; set; }
        public List<LocationEntity> Location { get; set; }

        public ConfigTool()
        {
            InitializeComponent();

            if (File.Exists(pathWorkshop))
            {
                try
                {
                    using (StreamReader r = new StreamReader(pathWorkshop))
                    {
                        string json = r.ReadToEnd();
                        Workshops = JsonConvert.DeserializeObject<List<WorkshopEntity>>(json);

                        if (Workshops != null)
                        {
                            r.Close();
                        }
                        else
                        {
                            Workshops = new List<WorkshopEntity>();
                        }
                    }
                }
                catch (Exception entry)
                {
                    MessageBox.Show(messageBoxText: "no JSON file found" + entry.Message);
                }
            }
            else
            {
                Workshops = new List<WorkshopEntity>();
            }//json workshop

            if (File.Exists(pathLocation))
            {
                try
                {
                    using (StreamReader r = new StreamReader(pathLocation))
                    {
                        string json = r.ReadToEnd();
                        Location = JsonConvert.DeserializeObject<List<LocationEntity>>(json);

                        if (Location != null)
                        {
                            r.Close();
                        }
                        else
                        {
                            Location = new List<LocationEntity>();
                        }
                    }
                }
                catch (Exception entry)
                {
                    MessageBox.Show(messageBoxText: "no JSON file found" + entry.Message);
                }
            }
            else
            {
                Location = new List<LocationEntity>();
            }
            DataContext = this;
        }

        private void ConfigSave_Click(object sender, RoutedEventArgs e)
        {
            string jsonLedgerWorkshop = JsonConvert.SerializeObject(Workshops);
            string jsonLedgerLocation = JsonConvert.SerializeObject(Location);

            try
            {
                using (StreamWriter outputFile = new StreamWriter(pathWorkshop, false))
                {
                    outputFile.Write(jsonLedgerWorkshop);
                }
            }
            catch (Exception entry)
            {
                MessageBox.Show(messageBoxText: "no JSON file found" + entry.Message);
            }
            try
            {
                using (StreamWriter outputFile = new StreamWriter(pathLocation, false))
                {
                    outputFile.Write(jsonLedgerLocation);
                }
            }
            catch (Exception entry)
            {
                MessageBox.Show(messageBoxText: "no JSON file found" + entry.Message);
            }
        }
    }
}
