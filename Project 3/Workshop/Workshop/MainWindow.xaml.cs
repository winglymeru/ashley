﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Workshop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string pathWorkshop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Workshops.json";
        private string pathLocation = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Location.json";

        public List<WorkshopEntity> WorkshopsList { get; set; }
        public List<LocationEntity> LocationList { get; set; }

        public int Total { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            if (File.Exists(pathWorkshop))
            {
                try
                {
                    using (StreamReader r = new StreamReader(pathWorkshop))
                    {
                        string json = r.ReadToEnd();
                        WorkshopsList = JsonConvert.DeserializeObject<List<WorkshopEntity>>(json);

                        if (WorkshopsList != null)
                        {
                            r.Close();
                        }
                        else
                        {
                            WorkshopsList = new List<WorkshopEntity>();
                        }
                    }
                }
                catch (Exception entry)
                {
                    MessageBox.Show(messageBoxText: "no JSON file found" + entry.Message);
                }
            }
            else
            {
                WorkshopsList = new List<WorkshopEntity>();
            }//pathWorkshop

            if (File.Exists(pathLocation))
            {
                try
                {
                    using (StreamReader r = new StreamReader(pathLocation))
                    {
                        string json = r.ReadToEnd();
                        LocationList = JsonConvert.DeserializeObject<List<LocationEntity>>(json);

                        if (LocationList != null)
                        {
                            r.Close();
                        }
                        else
                        {
                            LocationList = new List<LocationEntity>();
                        }
                    }
                }
                catch (Exception entry)
                {
                    MessageBox.Show(messageBoxText: "no JSON file found" + entry.Message);
                }
            }
            else
            {
                LocationList = new List<LocationEntity>();
            }
            DataContext = this;

        }//MainWindow

        private void CalculateCosts()
        {
            //Invoice invoice = new Invoice(WorkshopValue);
        }

        private void OpenConfigTool_Click(object sender, RoutedEventArgs e)
        {
            ConfigTool openConfigTool = new ConfigTool();
            openConfigTool.Show();
        }

        private void Workshop_Options_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            WorkshopEntity workshop = (WorkshopEntity)Workshop_Options.SelectedItem;
            LocationEntity location = (LocationEntity)Location_Options.SelectedItem;

            Invoice invoice = new Invoice(workshop, location);


            if (invoice.Total == 0)
            {
                Total_Cost.Text = "";
            }
            else
            {
                Total_Cost.Text = invoice.Total.ToString();
            }
        }

        private void Location_Options_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            WorkshopEntity workshop = (WorkshopEntity)Workshop_Options.SelectedItem;
            LocationEntity location = (LocationEntity)Location_Options.SelectedItem;

            Invoice invoice = new Invoice(workshop, location);

            Total_Cost.Text = invoice.Total.ToString();
        }
    }
}
